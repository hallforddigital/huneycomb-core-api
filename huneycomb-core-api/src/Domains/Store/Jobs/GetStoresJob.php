<?php

namespace App\Domains\Store\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Interfaces\CrudRepositoryInterface;

class GetStoresJob extends Job
{
    private $filter_by;
    private $filter_input_value;
    private $sort_by;

    public function __construct(string $filter_by, string $filter_input_value, string $sort_by)
    {
        $this->filter_by = $filter_by;
        $this->filter_input_value = $filter_input_value;
        $this->sort_by = $sort_by;
    }

    public function handle(CrudRepositoryInterface $store)
    {
        return $store->showAll(
            $this->filter_by,
            $this->filter_input_value,
            $this->sort_by
        );
    }
}
