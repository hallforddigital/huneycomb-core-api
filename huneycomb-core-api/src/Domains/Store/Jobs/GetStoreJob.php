<?php

namespace App\Domains\Store\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Interfaces\CrudRepositoryInterface;

class GetStoreJob extends Job
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function handle(CrudRepositoryInterface $store)
    {
        return $store->show($this->id);
    }
}
