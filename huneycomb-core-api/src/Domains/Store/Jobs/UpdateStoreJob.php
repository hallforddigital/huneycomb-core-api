<?php

namespace App\Domains\Store\Jobs;

use Lucid\Foundation\Job;

use App\Data\Repositories\Interfaces\CrudRepositoryInterface;

class UpdateStoreJob extends Job
{
    private $id;
    private $name;
    private $description;
    private $address;
    private $city;
    private $state;
    private $zip_code;
    private $country;
    private $website;

    public function __construct(int $id, string $name, string $description, string $address, string $city, string $state, string $zip_code, string $country, string $website)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->address = $address;
        $this->city = $city;
        $this->state = $state;
        $this->zip_code = $zip_code;
        $this->country = $country;
        $this->website = $website;
    }

    public function handle(CrudRepositoryInterface $store)
    {
        $data = array(
            'name' => $this->name,
            'description' => $this->description,
            'address' => $this->address,
            'city' => $this->city,
            'state' => $this->state,
            'zip_code' => $this->zip_code,
            'country' => $this->country,
            'website' => $this->website
        );

        return $store->update($this->id, $data);
    }
}
