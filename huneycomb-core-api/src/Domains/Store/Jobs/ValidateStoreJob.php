<?php

namespace App\Domains\Store\Jobs;

use Lucid\Foundation\Job;

use Illuminate\Support\Facades\Validator;

class ValidateStoreJob extends Job
{
    private $name;
    private $description;
    private $address;
    private $city;
    private $state;
    private $zip_code;
    private $country;
    private $website;

    public function __construct(string $name, string $description, string $address, string $city, string $state, string $zip_code, string $country, string $website)
    {
        $this->name = $name;
        $this->description = $description;
        $this->address = $address;
        $this->city = $city;
        $this->state = $state;
        $this->zip_code = $zip_code;
        $this->country = $country;
        $this->website = $website;
    }

    public function handle()
    {
        $data = [
            'name' => $this->name,
            'description' => $this->description,
            'address' => $this->address,
            'city' => $this->city,
            'state' => $this->state,
            'zip_code' => $this->zip_code,
            'country' => $this->country,
            'website' => $this->website
        ];

        $validator = Validator::make($data, [
            'name' => 'required|max:50',
            'description' => 'required|max:150',
            'address' => 'required|max:150',
            'city' => 'required|max:50',
            'state' => 'required|max:50',
            'zip_code' => 'required|max:12',
            'country' => 'required|max:50',
            'website' => array('required', 'regex:/([--:\w?@%&+~#=]*\.[a-z]{2,4}\/{0,2})((?:[?&](?:\w+)=(?:\w+))+|[--:\w?@%&+~#=]+)?/')
        ]);

        if($validator->fails()){
            return $validator->messages();
        } else {
            return true;
        }
    }
}
