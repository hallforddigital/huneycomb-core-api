<?php

namespace App\Data\Repositories\Store;

use App\Data\Store;
use App\Data\Repositories\Interfaces\CrudRepositoryInterface;
use Illuminate\Support\Facades\Schema;

class StoreRepository implements CrudRepositoryInterface
{
    /**
     * Get a store by ID
     * -----------------------------------------
     * @param integer $id = ID of the store
     */
    public function show(int $id): array
    {
        $result = Store::find($id);
        return $result->toArray();
    }

    /**
     * Get all stores
     */
    public function showAll(string $filter_by = '', string $filter_input_value = '', string $sort_by = ''): array
    {
        if ($filter_by == '' || $filter_input_value == '') {

            $store = Store::all();

        } else {

            $isColumnExist = Schema::hasColumn('stores', $filter_by);

            if ($isColumnExist) {

                $store = Store::where($filter_by, 'LIKE', "%{$filter_input_value}%")->get();

            } else {
                
                return [];
            }
        }

        $store = $store->sortBy($sort_by);

        return $store->toArray();
    }


    /**
     * Create a new Store
     * ----------------------------------------------------------------
     * @param array $data = Array of data for creating a new store
     */
    public function store(array $data): array
    {
        $result = Store::FirstOrCreate([
            'name' => $data['name']
        ], $data);

        return $result->toArray();
    }

    /**
     * Update a store
     * ---------------------------------------------
     * @param integer $id = ID of the store
     * @param array $data = array of data for updating store
     */
    public function update(int $id, array $data): array
    {
        $store = Store::find($id);

        if ($store) {

            if ($store->name !== $data['name']) $store->name = $data['name'];
            if ($store->description !== $data['description']) $store->description = $data['description'];
            if ($store->description !== $data['description']) $store->address = $data['address'];
            if ($store->city !== $data['city']) $store->city = $data['city'];
            if ($store->state !== $data['state']) $store->state = $data['state'];
            if ($store->zip_code !== $data['zip_code']) $store->zip_code = $data['zip_code'];
            if ($store->country !== $data['country']) $store->country = $data['country'];
            if ($store->website !== $data['website']) $store->website = $data['website'];

            if ($store->isDirty()) {
                $result = $store->save();
            } else {
                $result = false;
            }

            return $result ? $store->toArray() : [];

        } else {
            return [];
        }
    }

    /**
     * Delete a store
     * ---------------------------------
     * @param integer $id = store ID
     */
    public function destroy(int $id): array
    {
        $store = Store::find($id);

        if ($store) {
            $result = $store->delete();
        } else {
            $result = false;
        }

        return $result ? $store->toArray() : [];
    }
}
