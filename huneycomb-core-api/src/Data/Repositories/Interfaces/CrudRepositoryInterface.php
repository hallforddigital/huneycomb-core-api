<?php

namespace App\Data\Repositories\Interfaces;

interface CrudRepositoryInterface
{

    public function store(array $data);

    public function update(int $id, array $data);

    public function destroy(int $id): array;

    public function show(int $id): array;

    public function showAll(string $filter_by = '', string $filter_input_value = '', string $sort_by = ''): array;
}
