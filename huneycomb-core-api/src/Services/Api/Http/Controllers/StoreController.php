<?php

namespace App\Services\Api\Http\Controllers;

use App\Services\Api\Features\GetStoresFeature;
use App\Services\Api\Features\GetStoreFeature;
use App\Services\Api\Features\CreateStoreFeature;
use App\Services\Api\Features\UpdateStoreFeature;
use App\Services\Api\Features\DeleteStoreFeature;

use Lucid\Foundation\Http\Controller;

class StoreController extends Controller
{
    public function store()
    {
        return $this->serve(CreateStoreFeature::class);
    }

    public function update()
    {
        return $this->serve(UpdateStoreFeature::class);
    }

    public function getStore()
    {
        return $this->serve(GetStoreFeature::class);
    }

    public function getStores()
    {
        return $this->serve(GetStoresFeature::class);
    }

    public function destroy()
    {
        return $this->serve(DeleteStoreFeature::class);
    }
}
