<?php

namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;

use App\Domains\Store\Jobs\GetStoreJob;

class GetStoreFeature extends Feature
{
    public function handle(Request $request)
    {
        $getStore = $this->run(GetStoreJob::class, [
            'id' => isset($request->id) ? $request->id : NULL
        ]);
        
        if($getStore){
            $response = $this->run(new RespondWithJsonJob($getStore));
        } else {
            $response = $this->run(new RespondWithJsonErrorJob("Error while fetching store"));
        }

        return $response;
    }
}
