<?php

namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;

use App\Domains\Store\Jobs\GetStoresJob;

class GetStoresFeature extends Feature
{
    public function handle(Request $request)
    {
        $getStores = $this->run(GetStoresJob::class, [
            'filter_by' => isset($request->filter_by) ? $request->filter_by : '',
            'filter_input_value' => isset($request->filter_input_value) ? $request->filter_input_value : '',
            'sort_by' => isset($request->sort_by) ? $request->sort_by : '',
        ]);

        $response = $this->run(new RespondWithJsonJob($getStores));

        return $response;
    }
}
