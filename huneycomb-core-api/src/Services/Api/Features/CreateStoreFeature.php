<?php

namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;

use App\Domains\Store\Jobs\CreateStoreJob;
use App\domains\store\Jobs\ValidateStoreJob;

class CreateStoreFeature extends Feature
{
    public function handle(Request $request)
    {
        $validationResponse = $this->run(ValidateStoreJob::class, [
            'name' => isset($request->name) ? $request->name : NULL,
            'description' => isset($request->description) ? $request->description : NULL,
            'address' => isset($request->address) ? $request->address : NULL,
            'city' => isset($request->city) ? $request->city : NULL,
            'state' => isset($request->state) ? $request->state : NULL,
            'zip_code' => isset($request->zip_code) ? $request->zip_code : NULL,
            'country' => isset($request->country) ? $request->country : NULL,
            'website' => isset($request->website) ? $request->website : NULL
        ]);

        if($validationResponse === true) {

            $createStoreResult = $this->run(CreateStoreJob::class, [
                'name' => isset($request->name) ? $request->name : NULL,
                'description' => isset($request->description) ? $request->description : NULL,
                'address' => isset($request->address) ? $request->address : NULL,
                'city' => isset($request->city) ? $request->city : NULL,
                'state' => isset($request->state) ? $request->state : NULL,
                'zip_code' => isset($request->zip_code) ? $request->zip_code : NULL,
                'country' => isset($request->country) ? $request->country : NULL,
                'website' => isset($request->website) ? $request->website : NULL
            ]);

            if($createStoreResult)
            {
                $response = $this->run(new RespondWithJsonJob($createStoreResult));
            } else {
                $response = $this->run(new RespondWithJsonErrorJob("An error occured when trying to create a store"));
            }

        } else {
            $response = $this->run(new RespondWithJsonErrorJob($validationResponse));
        }

        return $response;
    }
}
