<?php

namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;

use App\Domains\Store\Jobs\UpdateStoreJob;
use App\Domains\Store\Jobs\ValidateStoreJob;

class UpdateStoreFeature extends Feature
{
    public function handle(Request $request)
    {
        $validationResponse = $this->run(ValidateStoreJob::class, [
            'name' => isset($request->name) ? $request->name : NULL,
            'description' => isset($request->description) ? $request->description : NULL,
            'address' => isset($request->address) ? $request->address : NULL,
            'city' => isset($request->city) ? $request->city : NULL,
            'state' => isset($request->state) ? $request->state : NULL,
            'zip_code' => isset($request->zip_code) ? $request->zip_code : NULL,
            'country' => isset($request->country) ? $request->country : NULL,
            'website' => isset($request->website) ? $request->website : NULL
        ]);

        if($validationResponse === true) {

            $updateStoreResult = $this->run(UpdateStoreJob::class, [
                'id' => isset($request->id) ? $request->id : NULL,
                'name' => isset($request->name) ? $request->name : NULL,
                'description' => isset($request->description) ? $request->description : NULL,
                'address' => isset($request->address) ? $request->address : NULL,
                'city' => isset($request->city) ? $request->city : NULL,
                'state' => isset($request->state) ? $request->state : NULL,
                'zip_code' => isset($request->zip_code) ? $request->zip_code : NULL,
                'country' => isset($request->country) ? $request->country : NULL,
                'website' => isset($request->website) ? $request->website : NULL
            ]);
    
            if($updateStoreResult)
            {
                $response = $this->run(new RespondWithJsonJob($updateStoreResult));
            } else {
                $response = $this->run(new RespondWithJsonErrorJob("An error occured when trying to update a store"));
            }

        } else {
            $response = $this->run(new RespondWithJsonErrorJob($validationResponse));
        }

        return $response;
    }
}
