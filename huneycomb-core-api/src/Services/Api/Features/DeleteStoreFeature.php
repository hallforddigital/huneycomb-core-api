<?php

namespace App\Services\Api\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;

use App\Domains\Http\Jobs\RespondWithJsonErrorJob;
use App\Domains\Http\Jobs\RespondWithJsonJob;

use App\Domains\Store\Jobs\DeleteStoreJob;

class DeleteStoreFeature extends Feature
{
    public function handle(Request $request)
    {
        $deleteStoreResult = $this->run(DeleteStoreJob::class, [
            'id' => isset($request->id) ? $request->id : NULL,
        ]);

        if($deleteStoreResult)
        {
            $response = $this->run(new RespondWithJsonJob($deleteStoreResult));
        } else {
            $response = $this->run(new RespondWithJsonErrorJob("An error occured when trying to delete a store"));
        }

        return $response;
    }
}
