<?php

/*
|--------------------------------------------------------------------------
| Service - API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Prefix: /api/api
Route::group(['prefix' => 'api'], function () {

    // The controllers live in src/Services/Api/Http/Controllers
    // Route::get('/', 'UserController@index');

    Route::get('/', function () {
        return response()->json(['path' => '/api/api']);
    });

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });


    Route::post('/store', 'StoreController@store');
    Route::put('/store', 'StoreController@update');
    Route::get('/stores/{filter_by?}/{filter_input_value?}/{sort_by?}', 'StoreController@getStores');
    Route::get('/store/{id}', 'StoreController@getStore');
    Route::delete('/store', 'StoreController@destroy');
});
